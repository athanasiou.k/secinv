#!/bin/bash


analyze_patch(){
    bm="OPT=${1} TOOLCHAIN=${2}"
    echo $bm
    # echo "secinv-2share-${2}-O${1} results/unpatched/"
    # make clean ${bm}
    # make batl-rules ${bm}
    # make batl-ila-tbl ${bm}
    # cp -r secinv-2share-${2}-O${1} results/unpatched/
    make batl-patch ${bm}
    make batl-rules ${bm}
    make batl-ila-tbl ${bm}
    cp -r secinv-2share-${2}-O${1} results/patched/
}

for opt in 0 1 2
do
    analyze_patch $opt gnu &
    #analyze_patch $opt llvm &
    #analyze_patch $opt gnu
done
    
