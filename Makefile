OPT = 2
SHARES = 2
INPUTS = 2
MUL = iswmul
TC = gnu
## The TBL ID to print rule details for.
ID = 0
LEAK = TBL
SAMPLES =
ifneq ($(SAMPLES),)
SAMPLEDEF = -D SAMPLE_SIZE=$(SAMPLES)
endif

SEED = $(shell shuf -i 0-4294967295 -n 1)
SEEDDEF = -D SEED=$(SEED)


DEFS = -D N=$(INPUTS)
DEFS += -D D=$(SHARES)
DEFS += $(SAMPLEDEF)
DEFS += $(SEEDDEF)
## The Target Binary Program
PRJ = secinv

## Directories: Source, Include, Build
SRCDIR = src
INCDIR = inc
BUILDDIR = $(PRJ)-$(SHARES)share-$(TC)-O$(OPT)

DEBUG = 0
## Flags, Toolchain names, Toolchain flags,  Load Scripts, Linker Flags,
## Assembler, Assembler Flags

CFLAGS = -c
CFLAGS += -g
CFLAGS += -Wall
CFLAGS += -Werror
CFLAGS += -O$(OPT)
CFLAGS += -ffreestanding
CFLAGS += -mthumb


ifeq ($(TC),gnu)
	ARM_GNU = arm-none-eabi
	CC = $(ARM_GNU)-gcc
	LD = $(CC)
	SIZE = $(ARM_GNU)-size
	DUMP = $(ARM_GNU)-objdump -D
	COPY = $(ARM_GNU)-objcopy
	CFLAGS += -nostdlib
	CFLAGS += -nostartfiles
	CFLAGS += -DQEMUDEBUG=$(DEBUG)
	CFLAGS += -mcpu=cortex-m3
	CFLAGS += -mfloat-abi=soft -mfpu=fpv4-sp-d16
	CFLAGS += -march=armv7-m 
	LDSCRIPT = $(SRCDIR)/flash.ld
	LDFLAGS = -T $(LDSCRIPT)
	AS = $(ARM_GNU)-as
	ASFLAGS += --warn --fatal-warnings -mcpu=cortex-m3 -mfloat-abi=soft -mfpu=fpv4-sp-d16
	LINKFLAGS = -mfloat-abi=soft -mfpu=fpv4-sp-d16 -nostartfiles
	QEMUASFLAGS = --defsym=QEMU=1
	QEMULINKFLAGS = -Wl,--defsym=QEMU=1 $(LDFLAGS)
else
	GDB = arm-none-eabi-gdb
	CC = clang-7
	LD = ld.lld-7
	SIZE = llvm-size-7
	DUMP = llvm-objdump-7 -triple=thumbv7em-unknown-none-eabi -disassemble-all -S -t
	COPY = llvm-objcopy-7
	CFLAGS += --target=thumbv7em-unknown-none-eabi
	CFLAGS += -mthumb
	CFLAGS += -march=armv7e-m
	CFLAGS += -mcpu=cortex-m3
	CFLAGS += -mfloat-abi=soft
	CFLAGS += -mfpu=fpv5-sp-d16
	CFLAGS += -std=c11
	LDSCRIPT = $(SRCDIR)/link.ld
	QEMUSCRIPT = $(SRCDIR)/link-qemu.ld
	LDFLAGS = --Bstatic
	LDFLAGS += --build-id
	LDFLAGS += --gc-sections
	LDFLAGS += --Map $(PRJ).map
	LDFLAGS += --script $(LDSCRIPT)
	LDFLAGS += -Llib
	AS = $(CC)
	ASFLAGS = $(filter-out -std=c11 -ffreestanding,$(CFLAGS))
	QEMUASFLAGS = -Wa,-defsym,QEMU=1
	QEMULINKFLAGS = $(filter-out --script $(LDSCRIPT),$(LDFLAGS))
	QEMULINKFLAGS += --script $(QEMUSCRIPT)
endif

## Includes
INC = -I$(INCDIR)
INCDEP = -I$(INCDIR)

## C source files and their objects.
SRC = $(shell find $(SRCDIR) -type f -name *.c)
OBJ = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SRC:.c=.o))
## ASM source files and their objects.
ASM_SRC = $(shell find $(SRCDIR) -type f -name *.S)
ASM_OBJ = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(ASM_SRC:.S=.o))
ASM_QEMU_SRC = $(shell find $(SRCDIR) -type f -name *qemu.asm)
ASM_QEMU_OBJ = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(ASM_QEMU_SRC:.asm=.o))
## Compiler generated ASM files.
ASM = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SRC:.c=.s))
QEMU_ASM=$(patsubst $(SRCDIR)/%,$(BUILDDIR)/$(LEAK)$(ID)/%, $(SRC:.c=.s))


QEMU_OBJ = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/$(LEAK)$(ID)/%,$(SRC:.c=.o))
QEMU_ASM_OBJ = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/$(LEAK)$(ID)/%,$(ASM_SRC:.S=.o))
%.o:%.c

.SECONDARY: $(ASM) $(PRJ).plp $(QEMU_ASM)

$(BUILDDIR)/%.o : $(SRCDIR)/%.S
	@mkdir -p $(dir $@)
	$(AS) $(ASFLAGS) $< -o $@

$(BUILDDIR)/ila.s : $(SRCDIR)/ila.c
	$(CC) $(filter-out -c -g -O$(OPT), $(CFLAGS)) -O2 $(INC) $(DEFS) -S $< -o $@


$(BUILDDIR)/%.s : $(SRCDIR)/%.c
	$(CC) $(filter-out -c -g, $(CFLAGS)) $(INC) $(DEFS) -S $< -o $@

$(BUILDDIR)/%.o : $(BUILDDIR)/%.s
	$(CC) $(filter-out -ffreestanding -std=c11, $(CFLAGS)) $(INC) $< -o $@

$(BUILDDIR)/$(PRJ).elf: $(ASM_OBJ) $(OBJ) 
	$(LD) $(LINKFLAGS) -o $@  $(LDFLAGS) $^
	$(DUMP) $(BUILDDIR)/$(PRJ).elf > $(BUILDDIR)/$(PRJ).lst || true
	$(COPY) $(BUILDDIR)/$(PRJ).elf $(BUILDDIR)/$(PRJ).bin -O binary
	$(SIZE) $(BUILDDIR)/$(PRJ).elf

$(BUILDDIR)/$(LEAK)$(ID)/%.o : $(SRCDIR)/%.S
	@mkdir -p $(dir $@)
	python -c "import eval.qemu_ila; eval.qemu_ila.print_aux_files(\"$(BUILDDIR)/$(PRJ).plp\", \"$(BUILDDIR)/$(PRJ).elf\", $(ID), \"$(LEAK)\")"
	$(AS) $(QEMUASFLAGS) $(ASFLAGS) $< -o $@

$(BUILDDIR)/$(LEAK)$(ID)/ila.s: $(SRCDIR)/ila.c 
	$(eval TMP := $(shell head -n 1 $(BUILDDIR)/$(LEAK)$(ID)/AUX))
	$(CC) $(filter-out -c -g -O$(OPT), $(CFLAGS)) -O2 $(INC) $(DEFS) $(TMP) -S $< -o $@; \
	$(eval S_TARGET := $(shell tail -n 1 $(BUILDDIR)/$(LEAK)$(ID)/AUX))
	@if [ "$(notdir $@)" = "$(S_TARGET)" ]; then\
		echo "Applying wrapper code for $(LEAK)$(ID) in $(S_TARGET)"; \
		echo "python -c \"import eval.qemu_ila; eval.qemu_ila.qemu_ila_of_lp(\"$(BUILDDIR)/$(PRJ).plp\", \"$(BUILDDIR)/$(PRJ).elf\",$(ID), \"$(LEAK)\")\"; "; \
		python -c "import eval.qemu_ila; eval.qemu_ila.qemu_ila_of_lp(\"$(BUILDDIR)/$(PRJ).plp\", \"$(BUILDDIR)/$(PRJ).elf\",$(ID), \"$(LEAK)\")"; \
	 fi; \


$(BUILDDIR)/$(LEAK)$(ID)/%.s: $(SRCDIR)/%.c 
	$(eval TMP := $(shell head -n 1 $(BUILDDIR)/$(LEAK)$(ID)/AUX))
	$(CC) $(filter-out -c -g, $(CFLAGS)) $(INC) $(DEFS) $(TMP) -S $< -o $@; \
	$(eval S_TARGET := $(shell tail -n 1 $(BUILDDIR)/$(LEAK)$(ID)/AUX))
	@if [ "$(notdir $@)" = "$(S_TARGET)" ]; then\
		echo "Applying wrapper code for $(LEAK)$(ID) in $(S_TARGET)"; \
		echo "python -c \"import eval.qemu_ila; eval.qemu_ila.qemu_ila_of_lp(\"$(BUILDDIR)/$(PRJ).plp\", \"$(BUILDDIR)/$(PRJ).elf\",$(ID), \"$(LEAK)\")\"; "; \
		python -c "import eval.qemu_ila; eval.qemu_ila.qemu_ila_of_lp(\"$(BUILDDIR)/$(PRJ).plp\", \"$(BUILDDIR)/$(PRJ).elf\",$(ID), \"$(LEAK)\")"; \
	 fi; \

$(BUILDDIR)/$(LEAK)$(ID)/%.o : $(BUILDDIR)/$(LEAK)$(ID)/%.s
	$(CC) $(filter-out -ffreestanding -std=c11, $(CFLAGS)) $(INC)  $< -o  $@

$(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).elf: $(QEMU_ASM_OBJ) $(QEMU_OBJ) 
	$(LD) $(QEMULINKFLAGS) $(LINKFLAGS) -o $@  $^
	$(DUMP) $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).elf > $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).lst || true
	$(COPY) $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).elf $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).bin -O binary
	$(SIZE) $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).elf



$(BUILDDIR)/$(LEAK)$(ID)/hw.out: $(BUILDDIR)/$(PRJ).plp $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).elf
	$ qemu-system-arm -nographic -machine lm3s6965evb -no-reboot -kernel $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).bin > $(BUILDDIR)/$(LEAK)$(ID)/hw.out

$(BUILDDIR)/$(LEAK)$(ID)/ila.out: $(BUILDDIR)/$(LEAK)$(ID)/hw.out
	python -c "import eval.qemu_ila; eval.qemu_ila.gather_results(\"$(BUILDDIR)/$(LEAK)$(ID)/hw.out\", \"$(BUILDDIR)/$(PRJ).plp\", $(ID), \"$(LEAK)\", $(SAMPLES))" > $(BUILDDIR)/$(LEAK)$(ID)/ila.out

### How to objdump in llvm: llvm-objdump-7 -x -s -S secinv.elf
bin: $(BUILDDIR)/$(PRJ).elf ## Build the project.



$(BUILDDIR)/$(PRJ).plp: $(BUILDDIR)/$(PRJ).elf
	@echo "Running static analysis on $(BUILDDIR)/$(PRJ).elf"
	python $(PRJ)_driver.py -elf_file $(BUILDDIR)/$(PRJ).elf -shares $(SHARES) -inputs $(INPUTS) -mul $(MUL) -out $(BUILDDIR)/$(PRJ).plp -cc $(TC) -opt $(OPT)


$(BUILDDIR)/$(PRJ)_elp_tbl.py: $(BUILDDIR)/$(PRJ).plp
	python -c "import batl.translator; batl.translator.executable_leaky_points(\"$(BUILDDIR)/$(PRJ).plp\", \"$(BUILDDIR)/$(PRJ)_elp_tbl.py\")"

$(BUILDDIR)/$(PRJ)_elp_vbl.py: $(BUILDDIR)/$(PRJ).plp
	python -c "import batl.translator; batl.translator.executable_leaky_points(\"$(BUILDDIR)/$(PRJ).plp\", \"$(BUILDDIR)/$(PRJ)_elp_vbl.py\")"


batl-qemu-ila-single: $(BUILDDIR)/$(LEAK)$(ID)/ila.out ## Compute ila of lp with ID=NUM TYPE=VBL|TBL via qemu.
	echo "ILA of $(LEAK)$(ID):"
	cat $(BUILDDIR)/$(LEAK)$(ID)/ila.out

batl-qemu-ila: $(BUILDDIR)/$(PRJ).plp
	python -c "import eval.ila; eval.ila.qemu_ila_tblps(\"$(BUILDDIR)\", $(OPT), \"$(TC)\", \"$(BUILDDIR)/$(PRJ).plp\")"

batl-python-ila: $(BUILDDIR)/$(PRJ).plp $(BUILDDIR)/$(PRJ)_elp_tbl.py $(BUILDDIR)/$(PRJ)_elp_vbl.py ## Lazily compute ILA values of TBLPs and store in plp file. 
	python -O -c "import eval.ila; eval.ila.ila_tblps(\"$(PRJ)_elp_vbl.py\", \"$(PRJ)_elp_tbl.py\", \"$(BUILDDIR)\", \"$(BUILDDIR)/$(PRJ).plp\")"

batl-python-ila-single: $(BUILDDIR)/$(PRJ).plp $(BUILDDIR)/$(PRJ)_elp_tbl.py $(BUILDDIR)/$(PRJ)_elp_vbl.py ## Compute the ILA values of  PLPs with ID=NUM TYPE=VBL|TBL
	python -O -c "import eval.ila; eval.ila.ila_of_lp(\"$(PRJ)_elp\", \"$(BUILDDIR)\", \"$(BUILDDIR)/$(PRJ).plp\", $(ID), \"$(LEAK)\")"

batl-python-ila-all: $(BUILDDIR)/$(PRJ).plp $(BUILDDIR)/$(PRJ)_elp_tbl.py $(BUILDDIR)/$(PRJ)_elp_vbl.py ## Compute ILA values of all PTBLs and PVBLs stored in plp file.
	python -c "import eval.ila; eval.ila.ila_values(\"$(PRJ)_elp_tbl.py\", \"$(BUILDDIR)\", \"$(BUILDDIR)/$(PRJ).plp\")"
	python -c "import eval.ila; eval.ila.ila_values(\"$(PRJ)_elp_vbl.py\", \"$(BUILDDIR)\", \"$(BUILDDIR)/$(PRJ).plp\")"

batl-analyze: $(BUILDDIR)/$(PRJ).plp ## Run BATL and store results of the analysis under project dir.

batl-analyze-repl: $(BUILDDIR)/$(PRJ).elf ## Initialize analysis and give repl.
	@echo "Initializing analysis and return in python repl."
	python -i $(PRJ)_driver.py -elf_file $(BUILDDIR)/$(PRJ).elf -shares $(SHARES) -inputs $(INPUTS) -mul $(MUL) -out $(BUILDDIR)/$(PRJ).plp -init_only

batl-ppPLPs: $(BUILDDIR)/$(PRJ).plp ## Pretty print PLPs.
	@echo "Printing Potential Leaky Point information"
	python -c "import batl.utils; batl.utils.print_plps(\"$(BUILDDIR)/$(PRJ).plp\", genuine_only=False)"

batl-rules: $(BUILDDIR)/$(PRJ).plp ## Apply leakage rules to all PLPs.
	python -c "import eval.tree_classification.leak_rules; eval.tree_classification.leak_rules.run_rules(\"$(BUILDDIR)/$(PRJ).plp\")"


batl-ila-tbl: batl-rules $(BUILDDIR)/$(PRJ).plp ## Lazily compute ILA values of TBLPs and store in plp file. 
	python -c "import batl.translator; batl.translator.executable_leaky_points(\"$(BUILDDIR)/$(PRJ).plp\", \"$(BUILDDIR)/$(PRJ)_elp.py\")"
	python -O -c "import eval.ila; eval.ila.ila_tblps(\"$(PRJ)_elp_vbl.py\", \"$(PRJ)_elp_tbl.py\", \"$(BUILDDIR)\", \"$(BUILDDIR)/$(PRJ).plp\")"

batl-patch: $(BUILDDIR)/$(PRJ).plp ## Patch the PLPs reported by the analysis.
	python -c "import batl.tbl_patcher; batl.tbl_patcher.patch_lps(\"$(BUILDDIR)/$(PRJ).plp\", \"$(BUILDDIR)/$(PRJ).elf\", genuine_only=True)" > $(BUILDDIR)/patch.out
.PHONY: clean help


batl-get-tbl: $(BUILDDIR)/$(PRJ).plp ## Return the PTBL with ID=NUM in an interactive python shell.
	python -c "import batl.utils; batl.utils.get_tbl(\"$(BUILDDIR)/$(PRJ).plp\", $(ID))"

batl-get-vbl: $(BUILDDIR)/$(PRJ).plp ## Return the PVBL with ID=NUM in an interactive python shell.
	python -c "import batl.utils; batl.utils.get_vbl(\"$(BUILDDIR)/$(PRJ).plp\", $(ID))"


qemu-run-debug: $(BUILDDIR)/$(PRJ).plp $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).elf # Start stalled qemu and wait for gdb
	qemu-system-arm -nographic -s -machine lm3s6965evb -no-reboot -S -kernel $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).bin

qemu-gdb: $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).bin # Attach gdb to stalled qemu. Use "jump ila" to start debuggin.
	$(GDB) -eval-command="rl" $(BUILDDIR)/$(LEAK)$(ID)/$(PRJ).elf

clean-qemu:  ## Delete qemu-build directory for leak with ID=NUM LEAK=VBL|TBL.
	@echo "Clean all from $(BUILDDIR)/$(LEAK)$(ID)"
	rm -r $(BUILDDIR)/$(LEAK)$(ID)

clean:  ## Delete build directory.
	@echo "Clean all from $(BUILDDIR)"
	rm -r $(BUILDDIR)

##rm -fR $(OBJ) $(ASM_OBJ) $(ASM) $(BUILDDIR)/$(PRJ).elf $(BUILDDIR)/$(PRJ).lst

-include $(OBJ:.o=.d)
.DEFAULT_GOAL := help
help:
	@echo "Usage: make [options] target"
	@echo "Options:"
	@echo "  SHARES=NUM\t\t Number of shares to use. Default: 2."
	@echo "  INPUTS=NUM\t\t Number of inputs to use. Default: 2."
	@echo "  MUL=iswmul|timul\t Which multiplication primitive to use. Default: iswmul."
	@echo "  OPT=0|1|2\t\t Which -O optimization level to use. Default: -O2."
	@echo "  TC=gnu|llvm\t Which compiler toolchain to use. Default: gnu."
	@echo ""
	@echo "Targets:"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[36m%-22s\033[0m %s\n", $$1, $$2}'
