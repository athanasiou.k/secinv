#include <stdint.h>
#include "stm32f103.h"
#include "invmul.h"
#include "KeccakRNG.h"
#include "pl011.h"
#include "ila.h"
extern unsigned char rand_array[200];
extern unsigned char rand_index;
extern unsigned char RetRand();
extern void InitRand(unsigned char *rand_state);


#ifndef NUM_RANDS
#define NUM_RANDS 0
#endif

#define WORD_SIZE 32
#define WORD_RANGE 256

#define UART0BASE 0x4000C000
uint8_t vhw[WORD_RANGE][WORD_SIZE+1];
/* float e[WORD_RANGE]; */
/* float f,v; */

void reset_timer(){
	unsigned int ra;
	
	ra = GET32(SCB_DEMCR);
	ra = ra | 0x01000000;
	PUT32(SCB_DEMCR, ra);
	PUT32(DWT_CYCCNT, 0);
	PUT32(DWT_CONTROL, 0);
}


void start_timer(){
	unsigned int ra;
	ra = GET32(DWT_CONTROL);
	ra = ra | 0x00000001;
	PUT32(DWT_CONTROL, ra);
	//    *DWT_CONTROL = *DWT_CONTROL | 1 ; // enable the counter
}

void stop_timer(){
	unsigned int ra;
	ra = GET32(DWT_CONTROL);
	ra = ra & 0xFFFFFFFE;
	PUT32(DWT_CONTROL, ra);
	//*DWT_CONTROL = *DWT_CONTROL | 0 ; // disable the counter    
}

unsigned int getCycles(){
	return GET32(DWT_CYCCNT);
}

void TriggerOn()
{
	unsigned int ra;
	ra = GET32(GPIOA_ODR);
	ra = ra & 0xFFFFFFFE;		//PA0 = 0
	PUT32(GPIOA_ODR,ra);
}

void TriggerOff()
{
	unsigned int ra;
	ra = GET32(GPIOA_ODR);
	ra = ra | 0x00000001;		//PA0 = 0
	PUT32(GPIOA_ODR,ra);
}

// PA0 is the pin for trigger
void SetupTrigger()
{
	unsigned int ra;
	ra = GET32(GPIOA_CRL);
	ra = ra & 0xFFFFFFF0;		//clear GPIOA_CRL, PA0 
	ra = ra | 0x00000003;		//output mode, 50MHZ, general push up
	PUT32(GPIOA_CRL,ra);
    
	TriggerOff();
}

int notmain(int argc, char *argv[])
{


	int i, j;
	unsigned int ra, num_cycles;

	ra=GET32(RCC_APB2ENR);
	ra|=1<<2;   //GPIOA
	ra|=1<<4;   //GPIOC
	ra|=1<<14;  //USART1
	PUT32(RCC_APB2ENR,ra);
	
	
	// setup UART1
	// pa9 TX  alternate function output push-pull
	// pa10 RX configure as input floating
	ra=GET32(GPIOA_CRH);
	ra&=~(0xFF0);
	ra|=0x490;
	PUT32(GPIOA_CRH,ra);
	
	PUT32(USART1_CR1,0x2000);
	PUT32(USART1_CR2,0x0000);
	PUT32(USART1_CR3,0x0000);
	//8000000/16 = 500000
	//500000/115200 = 4.34
	//4 and 5/16 = 4.3125
	//4.3125 * 16 * 115200 = 7948800
	PUT32(USART1_BRR,0x0045);
	PUT32(USART1_CR1,0x200C);
	
	PUT32(STK_CTRL,4);
	PUT32(STK_LOAD,0x00FFFFFF);
	PUT32(STK_CTRL,5);

	SetupTrigger();

	// Get SHA-3 PRNG seed through UART
	unsigned int KeccakState[5 * 5 * 2];
	unsigned int KeccakIn[5 * 5 * 2];
	unsigned char rand_state[200];
	for(i=0; i<50; i++)
	{
		while(1)
		{
			if(GET32(USART1_SR)&0x20)
				break;
		}
		ra=GET32(USART1_DR)&0xFF;
		
		KeccakState[i] = ra;
	}
	

	
	uint8_t p[N][D], k[N][D], c[N][D], x[D], one[D];
	uint8_t resinv, pk, resone;
	
	

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < D; j++)
		{
			p[i][j] = 0;
			k[i][j] = 0;
			c[i][j] = 0;
		}
	}
	for (j = 0; j < D; j++)
	{
		x[j] = 0;
		one[j] = 0;
	}
	
	
	
	
	while(1)
	{		
		//Re-generate random numbers using Keccak, and store them into random array
		for(i=0; i<50; i++)
		{
			KeccakIn[i] = KeccakState[i];			
		}
		KeccakF(KeccakState, KeccakIn, 25);

		rand_index = 0; 
		j = 0;
		for(i=0; i<50; i++)
		{
			rand_state[j] = KeccakState[i] & 0xFF;			j++;
			rand_state[j] = ( KeccakState[i] >>  8) & 0xFF;	j++;
			rand_state[j] = ( KeccakState[i] >> 16) & 0xFF;	j++;
			rand_state[j] = ( KeccakState[i] >> 24) & 0xFF;	j++;
		}
		InitRand(rand_state);





		
		secmuldef secmul = &iswmul;//timul;
		secinvdef secinv = &secinvlu;
		
		// init k0, k1
		for(i=0; i<N; i++)
		{
			while(1)
			{
				if(GET32(USART1_SR)&0x20)
					break;
			}
			k[i][D-1] = GET32(USART1_DR)&0xFF;
		}
		
		// init p0, p1
		for(i=0; i<N; i++)
		{
			while(1)
			{
				if(GET32(USART1_SR)&0x20)
					break;
			}
			p[i][D-1] = GET32(USART1_DR)&0xFF;
		}




		// init shares
		for (i = 0; i < N; i++)
		{
			for(j=0; j<D-1; j++)
			{
				p[i][j] = RetRand();
				k[i][j] = RetRand();
			}
			
			for (j = 0; j < D-1; j++)
			{
				p[i][D-1] ^= p[i][j];
				k[i][D-1] ^= k[i][j];
			}
		}
		
		// masked key addition and inversion 
		//for (i = 0; i < N; i++)
		i=0;		
		{
			// removing pk for better BATL. consider if needed when evaluating runtime.
			for (j = 0; j < D; j++)
				x[j] = p[i][j] ^ k[i][j];
			
			TriggerOn();
			reset_timer(); //reset timer		
			start_timer();
			//timul(x, x, c[i]);
			secinv(x, c[i], secmul);
			stop_timer();
			num_cycles = getCycles();
			TriggerOff();
			
			secmul(x, c[i], one);

			resinv = 0;
			resone = 0;
			pk = 0;
			for (j = 0; j < D; j++)
			{
				pk ^= x[j];
				resinv ^= c[i][j];
				resone ^= one[j];
			}
		}

		
		// Upload result through uart
		{
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, pk);
			
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, resinv);
			
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, resone);
			
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, num_cycles&0xff);
			
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, (num_cycles>>8)&0xff);
			
		
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, (num_cycles>>16)&0xff);
			
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, (num_cycles>>24)&0xff);
			
		}
	
		
        //for (i = 0; i < N; i++)
	/* i=1; */
	/* { */
	/*   pk = 0; */
	/*   for (j = 0; j < D; j++) */
        /*     { */
	/*       x[j] = p[i][j] ^ k[i][j]; */
	/*       pk ^= x[j]; */
        /*     } */
	/*   secinv(x, c[i], secmul); */
	/*   secmul(x, c[i], one); */
	/*   resinv = 0; */
	/*   resone = 0; */
	/*   for (j = 0; j < D; j++) */
        /*     { */
	/*       resinv ^= c[i][j]; */
	/*       resone ^= one[j]; */
        /*     } */
        /* } */

	}
	return 0;

}



unsigned mod(unsigned n, unsigned d)
{
	// n for numerator, d for denominator
	while (n >= d)
		n -= d;
	return n;
}

unsigned div(unsigned n, unsigned d)
{
	uint8_t w = 32;
	unsigned test_a, a, m, q, msb_tester;

	msb_tester = 1 << 31;
	a = 0;
	m = d;
	q = n;

	do { 
		a = (a << 1) ^ ((q >> 31) & 1);

		q = a << 1;
		test_a = a;
		
		a = a - m;
		
		if (a & msb_tester)
		{
			q &= ~1;
			a = test_a;
		}
		else
			q |= 1;
		w--;
	} while (w > 0);

	return q;
	
	
		
}

/* unsigned long long ipow(unsigned long base, unsigned long exp) */
/* { */
/*     unsigned long result = 1; */
/*     for (;;) */
/*     { */
/*         if (exp & 1) */
/*             result *= base; */
/*         exp >>= 1; */
/*         if (!exp) */
/*             break; */
/*         base *= base; */
/*     } */

/*     return result; */
/* } */


int qemumain()
{
    ila();
/* 	secmuldef secmul = &iswmul; */
/* 	secinvdef secinv = &secinvlu; */
    
/* 	uint8_t x[D], r[D],  popcnt, key_val, t; */
/* 	uint32_t res; */
/* 	uint8_t next_rands[NUM_RANDS]; */
/* 	unsigned space_size; */
/* 	unsigned i ,j, k; */
/* 	unsigned KeccakState[5 * 5 * 2]; */

/* 	/\* for(i=0; i<256; i++) *\/ */
/* 	/\* { *\/ */
/* 	/\* 	res = i; *\/ */
/* 	/\* 	res = res - ((res >> 1) & 0x55); *\/ */
/* 	/\* 	res = (res & 0x33) + ((res >> 2) & 0x33); *\/ */
/* 	/\* 	popcnt =  (((res + (res >> 4)) & 0x0F) * 0x01); *\/ */
/* 	/\* 	putuint8_t(i); *\/ */
/* 	/\* 	putchar('\t'); *\/ */
/* 	/\* 	putuint8_t(popcnt); *\/ */
/* 	/\* 	putchar('\n'); *\/ */
		
/* 	/\* } *\/ */


/* 	j = div(11, 3); */
/* 	for(i=0; i<j; i++) */
/* 	{ */
/* 		KeccakState[i] = 0; */
/* 	} */

/* 	myInitRand(KeccakState); */

/* 	for (i=0; i< WORD_RANGE; i++) */
/* 		for (j=0; j< WORD_SIZE +1 ; j++) */
/* 		    vhw[i][j]=0; */

/* 	/\* for (i=0; i< WORD_RANGE; i++) *\/ */
/* 	/\* { *\/ */
/* 	/\*     for (j=0; j< WORD_SIZE +1 ; j++) *\/ */
/* 	/\*     { *\/ */
/* 	/\* 	putuint8_t(vhw[i][j]); *\/ */
/* 	/\* 	putchar('\t'); *\/ */
/* 	/\*     } *\/ */
/* 	/\* putchar('\n'); *\/ */
/* 	/\* } *\/ */


/* 	/\* next_rands[0] = 0; *\/ */
/* 	/\* x[0] = 1; *\/ */
/* 	/\* x[1] = 0; *\/ */
/* 	/\* SetNextNRands(100, next_rands); *\/ */
/* 	/\* secinv(x, r, secmul); *\/ */
/* 	space_size = ipow(WORD_RANGE, D+NUM_RANDS); */
/* 	t = (1/ ipow(WORD_RANGE, 1)) % WORD_RANGE; */
/* 	if (D+NUM_RANDS <= 3) */
/* 	{ */
/* 		for (i=0; i < space_size; i++ ) */
/* 		{ */
/* 			leakage_visit_counter = 0; */
/* 			res=0; */
/* 			for (j=0; j < NUM_RANDS; j++) */
/* 			{ */
/* 				// Crucial cast to allow usage of udiv */
/* 				t = (i / ((int)ipow(WORD_RANGE, j))) % WORD_RANGE; */
/* 				next_rands[j] = t; */
/* #ifdef QEMUDEBUG */
/* #if (QEMUDEBUG > 0) */
/* 				putuint8_t(t); */
/* 				putchar('\t'); */
/* #endif */
/* 			} */
/* 			key_val =0; */
/* 			for (j=NUM_RANDS; j < NUM_RANDS+D; j++) */
/* 			{ */
/* 				// Crucial cast to allow usage of udiv */
/* 				t =(i / ((int)ipow(WORD_RANGE, j))) % WORD_RANGE; */
/* #if (QEMUDEBUG > 0) */
/* 				putuint8_t(t); */
/* 				putchar('\t'); */
/* #endif */
/* 				x[j-NUM_RANDS] = t; */
/* 				key_val ^= x[j-NUM_RANDS]; */
/* 			} */
/* #if (QEMUDEBUG > 0) */
/* 			putuint8_t(key_val); */
/* 			putchar('\t'); */
/* #endif */
/* 			SetNextNRands(200, next_rands); */
/* 			secinv(x, r, secmul); */

/* 			res = leakage_value; */
/* #if (QEMUDEBUG > 0) */
/* 			putunsigned(res); */
/* 			putchar('\t'); */
/* 			putchar('\n'); */
/* #endif */
/* #endif */
/* 			popcnt = popcount_32(res); */
		    
/* 			if (vhw[key_val][popcnt] == 255) */
/* 			{ */
/* 				putuint8_t(key_val); */
/* 				putchar(','); */
/* 				putuint8_t(popcnt); */
/* 				putchar(':'); */
/* 				putchar(0x30+2); */
/* 				putchar(0x30+5); */
/* 				putchar(0x30+6); */
/* 				putchar('\n'); */
/* 			} */
/* 			vhw[key_val][popcnt]++; */
/* 		} */
/* 	} */
/* 	else */
/* 	{ */
/* 		for (i = 0; i < WORD_RANGE; i++) */
/* 		{ */
/* 			x[D-1] = i; */
/* 			for (k = 0; k < SAMPLE_SIZE; k++) */
/* 			{ */
/* 				res=0; */
/* 				for (j = 0; j < D-1; j++) */
/* 					x[j] = RetRand(); */
/* 				key_val =0; */
/* 				for (j = 0; j < D; j++) */
/* 					key_val ^= x[j]; */
/* 				secinv(x, r, secmul); */
/* 				res = leakage_value; */
				
/* 				popcnt = popcount_32(res); */
/* 				if (vhw[key_val][popcnt] == 255) */
/* 				{ */
/* 					putuint8_t(key_val); */
/* 					putchar(','); */
/* 					putuint8_t(popcnt); */
/* 					putchar(':'); */
/* 					putchar(0x30+2); */
/* 					putchar(0x30+5); */
/* 					putchar(0x30+6); */
/* 					putchar('\n'); */
/* 				} */
/* 				vhw[key_val][popcnt]++; */
/* 			} */
/* 		} */
/* 	} */
	
	
/* 	for (i = 0; i < WORD_RANGE; i++) */
/* 	{ */
/* 		for (j = 0; j < WORD_SIZE+1; j++) */
/* 		{ */
/* 			putuint8_t(i); */
/* 			putchar(','); */
/* 			putuint8_t(j); */
/* 			putchar(':'); */
/* 			putuint8_t(vhw[i][j]); */
/* 			putchar('\n'); */
/* 		} */
/* 	} */
	
	// RESET code to exit from qemu
#define SYSRESETREQ    (1<<2)
#define VECTKEY        (0x05fa0000UL)
#define VECTKEY_MASK   (0x0000ffffUL)
#define AIRCR          (*(uint32_t*)0xe000ed0cUL) // fixed arch-defined address
#define REQUEST_EXTERNAL_RESET (AIRCR=(AIRCR&VECTKEY_MASK)|VECTKEY|SYSRESETREQ)

	REQUEST_EXTERNAL_RESET;

    return(0);

}
  

