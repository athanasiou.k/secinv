#include <stdint.h>
#include "invmul.h"
#include "KeccakRNG.h"
#include "pl011.h"

#ifndef SEED
#define SEED 0
#endif

#ifndef NUM_RANDS
#define NUM_RANDS 0
#endif

#define WORD_SIZE 32
#define WORD_RANGE 256
#ifndef SAMPLE_SIZE
#define SAMPLE_SIZE 10000
#endif

#define UART0BASE 0x4000C000
uint8_t vhw[WORD_RANGE][WORD_SIZE+1];

#if defined (__GNU__C) || defined(__GNUG__)
#pragma GCC push_options
#pragma GCC optimize ("O2")
#endif
unsigned long long ipow(unsigned long base, unsigned long exp) 
{
    unsigned long result = 1;
    for (;;)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }

    return result;
}

void ila()
{
    
	secmuldef secmul = &iswmul;
	secinvdef secinv = &secinvlu;
	
	uint8_t x[D], r[D],  popcnt, key_val, t;
	uint32_t res;
	uint8_t next_rands[NUM_RANDS];
	unsigned space_size;
	unsigned i ,j, k;
	unsigned KeccakState[5 * 5 * 2];

	/* for(i=0; i<256; i++) */
	/* { */
	/* 	res = i; */
	/* 	res = res - ((res >> 1) & 0x55); */
	/* 	res = (res & 0x33) + ((res >> 2) & 0x33); */
	/* 	popcnt =  (((res + (res >> 4)) & 0x0F) * 0x01); */
	/* 	putuint8_t(i); */
	/* 	putchar('\t'); */
	/* 	putuint8_t(popcnt); */
	/* 	putchar('\n'); */
		
	/* } */


	for(i=0; i<50; i++)
	{
		KeccakState[i] = SEED;
	}

	myInitRand(KeccakState);

	for (i=0; i< WORD_RANGE; i++)
		for (j=0; j< WORD_SIZE +1 ; j++)
		    vhw[i][j]=0;

	/* for (i=0; i< WORD_RANGE; i++) */
	/* { */
	/*     for (j=0; j< WORD_SIZE +1 ; j++) */
	/*     { */
	/* 	putuint8_t(vhw[i][j]); */
	/* 	putchar('\t'); */
	/*     } */
	/* putchar('\n'); */
	/* } */


	/* next_rands[0] = 0; */
	/* x[0] = 1; */
	/* x[1] = 0; */
	/* SetNextNRands(100, next_rands); */
	/* secinv(x, r, secmul); */
	space_size = ipow(WORD_RANGE, D+NUM_RANDS);
	t = (1/ ((int)ipow(WORD_RANGE, 1))) % WORD_RANGE;
	if (D+NUM_RANDS <= 3)
	{
		for (i=0; i < space_size; i++ )
		{
			leakage_visit_counter = 0;
			res=0;
			for (j=0; j < NUM_RANDS; j++)
			{
				// Crucial cast to allow usage of udiv
				t = (i / ((int)ipow(WORD_RANGE, j))) % WORD_RANGE;
				next_rands[j] = t;
#ifdef QEMUDEBUG
#if (QEMUDEBUG > 0)
				putuint8_t(t);
				putchar('\t');
#endif
#endif
			}
			key_val =0;
			for (j=NUM_RANDS; j < NUM_RANDS+D; j++)
			{
				// Crucial cast to allow usage of udiv
				t =(i / ((int)ipow(WORD_RANGE, j))) % WORD_RANGE;
#ifdef QEMUDEBUG
#if (QEMUDEBUG > 0)
				putuint8_t(t);
				putchar('\t');
#endif
#endif
				x[j-NUM_RANDS] = t;
				key_val ^= x[j-NUM_RANDS];
			}
#ifdef QEMUDEBUG
#if (QEMUDEBUG > 0)
			putuint8_t(key_val);
			putchar('\t');
#endif
#endif
			SetNextNRands(200, next_rands);
			secinv(x, r, secmul);

			res = leakage_value;
#ifdef QEMUDEBUG
#if (QEMUDEBUG > 0)
			putunsigned(res);
			putchar('\t');
			putchar('\n');
#endif
#endif
			popcnt = popcount_32(res);
		    
			if (vhw[key_val][popcnt] == 255)
			{
				putuint8_t(key_val);
				putchar(',');
				putuint8_t(popcnt);
				putchar(':');
				putchar(0x30+2);
				putchar(0x30+5);
				putchar(0x30+6);
				putchar('\n');
			}
			vhw[key_val][popcnt]++;
		}
	}
	else
	{
		for (i = 0; i < WORD_RANGE; i++)
		{
			key_val = i;
			x[D-1] = key_val;
			for (k = 0; k < SAMPLE_SIZE; k++)
			{
				res=0;
				leakage_visit_counter = 0;

				for (j = 0; j < D-1; j++)
				{
					x[j] = RetRand();
					x[D-1] ^= x[j];
				}
				secinv(x, r, secmul);
				res = leakage_value;
				
				popcnt = popcount_32(res);
				if (vhw[key_val][popcnt] == 255)
				{
					putuint8_t(key_val);
					putchar(',');
					putuint8_t(popcnt);
					putchar(':');
					putchar(0x30+2);
					putchar(0x30+5);
					putchar(0x30+6);
					putchar('\n');
				}
				vhw[key_val][popcnt]++;
			}
		}
	}
	
	
	for (i = 0; i < WORD_RANGE; i++)
	{
		for (j = 0; j < WORD_SIZE+1; j++)
		{
			putuint8_t(i);
			putchar(',');
			putuint8_t(j);
			putchar(':');
			putuint8_t(vhw[i][j]);
			putchar('\n');
		}
	}
	return;
}

#if defined (__GNU__C) || defined(__GNUG__)
#pragma GCC pop_options
#endif

