#ifndef _KeccakRNG_h_
#define _KeccakRNG_h_

typedef unsigned int u32;
typedef unsigned char u8;

typedef unsigned int UINT32;

void KeccakF( UINT32 * state, const UINT32 *in, int laneCount );

unsigned char RetRand();
void InitRand(unsigned char *rand_state);
void myInitRand(unsigned int *rand_state);
void SetNextNRands(u8 n, u8 * x);


#endif
