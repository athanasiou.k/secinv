#ifndef _STM32F103_H_
#define _STM32F103_H_ 

//PA9  TX
//PA10 RX

void PUT32 ( unsigned int, unsigned int );
unsigned int GET32 ( unsigned int );
unsigned int GET16 ( unsigned int );
void dummy ( unsigned int );


#define GPIOC_BASE 0x40011000
#define GPIOC_CRL  (GPIOC_BASE+0x00)
#define GPIOC_CRH  (GPIOC_BASE+0x04)
#define GPIOC_IDR  (GPIOC_BASE+0x08)
#define GPIOC_ODR  (GPIOC_BASE+0x0C)
#define GPIOC_BSRR (GPIOC_BASE+0x10)
#define GPIOC_BRR  (GPIOC_BASE+0x14)
#define GPIOC_LCKR (GPIOC_BASE+0x18)

#define STK_CTRL (0xE000E010+0x00)
#define STK_LOAD (0xE000E010+0x04)
#define STK_VAL  (0xE000E010+0x08)

#define USART1_BASE 0x40013800
#define USART1_SR   (USART1_BASE+0x00)
#define USART1_DR   (USART1_BASE+0x04)
#define USART1_BRR  (USART1_BASE+0x08)
#define USART1_CR1  (USART1_BASE+0x0C)
#define USART1_CR2  (USART1_BASE+0x10)
#define USART1_CR3  (USART1_BASE+0x14)
#define USART1_GTPR (USART1_BASE+0x18)

#define GPIOA_BASE 0x40010800
#define GPIOA_CRL  (GPIOA_BASE+0x00)
#define GPIOA_CRH  (GPIOA_BASE+0x04)
#define GPIOA_IDR  (GPIOA_BASE+0x08)
#define GPIOA_ODR  (GPIOA_BASE+0x0C)
#define GPIOA_BSRR (GPIOA_BASE+0x10)
#define GPIOA_BRR  (GPIOA_BASE+0x14)
#define GPIOA_LCKR (GPIOA_BASE+0x18)

#define RCC_BASE 0x40021000
#define RCC_APB2ENR (RCC_BASE+0x18)

#define DWT_CYCCNT  0xE0001004 
#define DWT_CONTROL 0xE0001000 
#define SCB_DEMCR   0xE000EDFC 


#endif
