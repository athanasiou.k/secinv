#ifndef _invmul_h_
#define _invmul_h_

#ifndef N
#define N 2
#endif
#ifndef D
#define D 2
#endif
/*
  Use a function ptr for different secure multiplication schemes
*/
typedef void (*secmuldef)(uint8_t*, uint8_t*, uint8_t*);
typedef void (*secinvdef)(uint8_t*, uint8_t*, secmuldef);

void iswmul(uint8_t * x, uint8_t * y, uint8_t * w);
void timul(uint8_t * x, uint8_t * y, uint8_t * w);
void testmul(uint8_t a, uint8_t b, secmuldef secmul);
void secinvlu(uint8_t * x, uint8_t * y, secmuldef secmul);
void secinvm(uint8_t * x, uint8_t * y, secmuldef secmul);
uint8_t spamul(uint8_t a, uint8_t b);

extern uint8_t leakage_visit_counter;
extern uint32_t leakage_value;

#endif
