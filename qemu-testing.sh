#!/bin/bash

# Code from:
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

ARGNUM=$#

for i in "$@"
do
case $i in
    -o=*|--opt=*)
    OPT="${i#*=}"
    shift # past argument=value
    ;;
    -l=*|--leaktype=*)
    LEAKTYPE="${i#*=}"
    shift # past argument=value
    ;;
    -n=*|--numleaks=*)
    NUMLEAKS="${i#*=}"
    shift # past argument=value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
          # unknown option
    ;;
esac
done

echo "OPTIMIZATION    = ${OPT}"
echo "LEAKTYPE        = ${LEAKTYPE}"
echo "NUMLEAKS        = ${NUMLEAKS}"
echo "DEFAULT         = ${DEFAULT}"
# echo "Number files in SEARCH PATH with EXTENSION:" $(ls -1 "${SEARCHPATH}"/*."${EXTENSION}" | wc -l)
# if [[ -n $1 ]]; then
#     echo "Last line of file specified as non-opt/last argument:"
#     tail -1 $1
# fi

usage="$(basename "$0") [-h] -o=opt -l=leakaget_type -n=leakage_numbers  -- program to test qemu and python ILA outputs

where:
    -h  show this help text
    -o, --opt          the optimization level of the analyzed binary (0,1,2 default: 2)
    -l, --leaktypype   the type of leakage to consider (VBL, TBL, default: TBL)
    -n, --numleaks     the number of leaks to test"

if [ "$ARGNUM" -ne 3 ]; then
    echo "Illegal number of parameters"
    echo "Usage:"
    echo "$usage"
    exit
fi


for (( i=0; i<=$NUMLEAKS; i++ ))
do
    echo "Testing $LEAKTYPE$i"
#   PYILARES=`make batl-python-ila-single OPT=$OPT ID=$i LEAK=$LEAKTYPE | tail -n 1 | awk '{print $NF}'`
#   PYILA=`time make batl-python-ila-single OPT=$OPT ID=$i LEAK=$LEAKTYPE 2>`
#   PYILARES=`echo $PYILA | tail -n 1 | awk '{print $NF}'`
#   PYILATIME=`echo $PYILA | awk 'NR==1{print}'`
#   echo $PYILA
    QEMUILARES=`make batl-qemu-ila-single OPT=$OPT ID=$i LEAK=$LEAKTYPE | tail -n 1| awk '{print $NF}'`
#    echo "Python:${PYILARES}"
    echo "QEMU  :${QEMUILARES}"
#   if [ $PYILARES == $QEMUILARES ]; then
# 	echo "PASS"
#   else
# 	echo "FAIL"
#   fi
done
