A C implementation of the secure inversion algorithm as described in:

`Matthieu Rivain and Emmanuel Prouff. Provably secure higher-order masking of aes. Cryptology ePrint Archive, Report 2010/441, 2010. https://eprint.iacr. org/2010/441`.

Target is the ARM cortex-M3 family of processors.


For the pinata cortex-M4 for processor, compile iswmul.c to assembly with the following command
`arm-none-eabi-gcc -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -Wall -ffunction-sections -g -S -O2 invmul.c`
