import angr
import batl.utils
import batl.tbl_detector
import batl.secinv_helpers
from batl.uf_app import SimConcretizationStrategyUfApp
from functools import reduce
import os
import sys
import pdb
import argparse
import pickle
import IPython
from collections import defaultdict
import time
import json

parser = argparse.ArgumentParser(description='BATL driver for secinv.')
parser.add_argument("-elf_file", help="Binary of secinv to analyze.", type=str, required=True)
parser.add_argument("-shares", help="Number of masking shares.", type=int, required=True)
parser.add_argument("-inputs", help="Number of keys/plaintexts given to secinv as input.", type=int, required=True)
parser.add_argument("-mul", help="The multiplication primitive considered.", type=str, required=True)
parser.add_argument("-out", help="The output file to store PLPs.", type=str, required=True)
parser.add_argument("-init_only", help="Stop after init, don't do analysis", action='store_true')
parser.add_argument("-cc", help="The toolchain that compiled the binary.", choices=['llvm', 'gnu'], type=str)
parser.add_argument("-opt", help="The optimization level used in compilation.", type=str, choices=['0', '1', '2'])
args = parser.parse_args()

dirname = os.path.dirname(os.path.abspath('__file__'))

elf_file = args.elf_file
dirname = os.path.dirname(os.path.abspath(elf_file))

proj = angr.Project(elf_file)

# Stubs for Random Number Generation routines.
proj.hook_symbol('RetRand', batl.secinv_helpers.RetRand())
proj.hook_symbol('KeccakF', batl.secinv_helpers.KeccakF())

# Stub for Input reading routines. Responsible for labeling the secret.
proj.hook_symbol('GET32', batl.secinv_helpers.GET32())

to_remove = {angr.options.SIMPLIFY_REGISTER_WRITES,
             angr.options.SIMPLIFY_REGISTER_READS,
             angr.options.CONSERVATIVE_READ_STRATEGY,
             angr.options.OPTIMIZE_IR}

to_add = {angr.options.TRACK_OP_ACTIONS,
          angr.options.TRACK_REGISTER_ACTIONS}.union(angr.options.refs)



mul_addr = proj.loader.find_symbol(args.mul).rebased_addr

notmain_addr = proj.loader.find_symbol("notmain").rebased_addr
start_addr = proj.loader.find_symbol("_start").rebased_addr
secinvlu_addr = proj.loader.find_symbol("secinvlu").rebased_addr
entry_state = proj.factory.call_state(start_addr,
                                      add_options=to_add,
                                      remove_options=to_remove)

usart_base = 0x40013800
usart_dr = usart_base + 0x04


entry_state.globals['rnd_cnt'] = 0
entry_state.globals['get_cnt'] = 0
entry_state.globals['init_rnd']= 50
entry_state.globals['init_key'] = args.inputs
entry_state.globals['init_txt'] = args.inputs
entry_state.globals['num_shares'] = args.shares
entry_state.globals['share_creation_xors'] =  2*(entry_state.globals['num_shares'])
entry_state.globals['key_txt_xors'] =  entry_state.globals['num_shares'] 
entry_state.globals['regs_blacklist'] = [72, 76, 80, 84]
entry_state.globals['xor_cnt'] = 0
entry_state.globals['shares_bvs'] = [[]]
entry_state.globals['elf_file'] = elf_file
entry_state.globals['llvm_opt'] = True if args.cc=='llvm' and args.opt=='2' else False
entry_state.globals['addr_cnt'] = defaultdict(int)
entry_state.globals['cnt'] = 0
entry_state.globals['cnt2addr'] = defaultdict(int)
entry_state.globals['addr2dead'] = defaultdict(int)



## Enabling share creation breakpoint.
shares_bp = entry_state.inspect.b('reg_write', when=angr.BP_AFTER,
                               condition=batl.secinv_helpers.is_xor,
                               action=batl.secinv_helpers.cnt_xors)

simgr = proj.factory.simgr(entry_state)

iswfun=proj.kb.functions.function(name='iswmul')
secinvlufun=proj.kb.functions.function(name='secinvlu')

start_time=time.time()
while simgr.active[0].addr != secinvlu_addr:
    simgr.step(num_inst=1)


## Enabling TBL detector.
print("Disabling shares-reading plugin, enabling TBL plugin.")
simgr.active[0].inspect.remove_breakpoint('reg_write',bp=shares_bp)
tbl_plugin = batl.tbl_detector.TBLPlugin(entry_state.solver.BVV(0, 32),
                                         simgr.active[0].globals['shares_bvs'],
                                         proj.arch.register_names,
                                         vbl=True)

simgr.active[0].register_plugin('tbl_plugin', tbl_plugin)

symbol_addresses = {}
for symbol in proj.loader.main_object.symbols:
    if symbol.section is None:
        continue
    if proj.loader.main_object.sections.raw_list[symbol.section].name != ".rodata":
        continue
    s_name = symbol.demangled_name
    s_addr_bv = simgr.active[0].solver.BVV(symbol.rebased_addr, 32)
    symbol_addresses[symbol.rebased_addr] = s_name, s_addr_bv

uf_app_strat = SimConcretizationStrategyUfApp(1024, symbol_addresses, simgr.active[0].solver.BVV(0, 32))
simgr.active[0].memory.read_strategies.insert(0, uf_app_strat)

simgr.active[0].inspect.b('reg_write', when=angr.BP_BEFORE,
                      action=batl.tbl_detector.update_regs)
simgr.active[0].inspect.b('reg_write', when=angr.BP_AFTER,
                      action=batl.tbl_detector.update_tbl)

if not args.init_only:

    ## Starting Leakage Detection.
    in_mul = False
    stop_addr = None
    while True:
        state = simgr.active[0]
        print("Exploring state with addr", hex(state.addr))
        if state.addr == stop_addr:
            print("Found stop addr")
            break
        state.globals['addr_cnt'][state.addr] += 1
        state.globals['cnt'] += 1
        state.globals['cnt2addr'][state.globals['cnt']] = state.addr
        if state.addr in state.globals['addr2dead']:
            state.globals['addr2dead'][state.addr] = state.globals['addr2dead'][state.addr] + [ set() ]
        else:
            state.globals['addr2dead'][state.addr] = [ set() ]
        simgr.step(num_inst=1)
        if not in_mul:
            if state.callstack.current_function_address == secinvlu_addr:
                stop_addr = state.callstack.ret_addr
                in_mul = True
    symex_elapsed_time = time.time() - start_time

    for lp in state.tbl_plugin.leaky_points:
        lp.total_visit_cnt = state.globals['addr_cnt'][lp.new_write_addr]
        dead_reg_lst = state.globals['addr2dead'][lp.new_write_addr]
        dead_regs = set.intersection(*dead_reg_lst)
        if dead_regs:
            print("PTBL{0}".format(lp.lp_id))
            print("dead_regs:{0}".format(dead_regs))
            lp.patch_reg = dead_regs.pop()
            print("reg {0} can patch PLPP{1}".format(lp.patch_reg,lp.lp_id))
        else:
            print("no dead reg found for PLPP{0}".format(lp.lp_id))

    print("Storing plps in", args.out)
    plps=(state.tbl_plugin.leaky_points, state.tbl_plugin.vbl_leaky_points)
    with open(args.out, 'wb') as f:
        pickle.dump(plps, f)
    batl.utils.print_plps(args.out)

    
    data={}
    data['symex_t'] = symex_elapsed_time
    profile_data_f = os.path.join(dirname, "profile_data.txt")
    with open(profile_data_f, 'w') as jsonfile:
        json.dump(data, jsonfile)

